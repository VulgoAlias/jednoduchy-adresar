<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', 'DictionaryController@index');

$router->get('/create/', 'DictionaryController@create');
$router->post('/create/', 'DictionaryController@create');

$router->get('/{nick}', 'DictionaryController@edit');
$router->post('/{nick}', 'DictionaryController@edit');

$router->get('/delete/{nick}', 'DictionaryController@delete');
