<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class DictionaryController extends Controller
{
				/**
				 * Defines all possible record types, and its options (just display names for now)
				 * @var array  
				 */
				public $categories;			
				
				/**
				 * Sets path to the dictionary JSON file.
				 * @var string
				 */
				private $dictionary_file;
				
				public function __construct()
				{
								$this->categories = 
								[
												'name' => 'Jméno a příjmení', 
												'email' => 'Email', 
												'phone' => 'Telefon', 
												'long_note' => 'Poznámka'
								];								
								
								$this->dictionary_file = base_path().'/sample/dictionary.json';
				}
				
				/**
				 * Shows a list of all contacts.
				 */
    public function index()
				{
								$array = $this->getList();

								if($array)
								{
												$view = base_path().'/views/home.php';
												$this->callView($view, ['array' => $array]);
								}
				}
				
				/**
				 * Creates a new contact record in the dictionary JSON file.
				 * @param Request $request
				 * @return object Instance of Redirector class.
				 */
				public function create(Request $request)
				{
								if($request->method() == 'POST')
								{
												$inputs = $this->sanitize($request->all());											
												$contact_exists = $this->getContact($inputs['nickname']);
												
												if($contact_exists)
												{
																$this->showError();
												}
												else
												{
																$record = $this->saveRecord($inputs);

																if($record)
																{
																				return redirect('/');
																}
																else
																{
																				$this->showError();
																}																
												}
								}
								else
								{
												$path = base_path()."/views/create.php";
												$data['sections'] = $this->categories;
												$data['sections']['nickname'] = 'Přezdívka';

												$this->callView($path,	$data);
								}
				}
				
				/**
				 * Allows user to edit contact's details.
				 * @param Request $request
				 * @param string $nick
				 * @return object Instance of Redirector class.
				 */
				public function edit(Request $request, $nick)
				{
								if($request->method() == 'POST')
								{
												$inputs = $this->sanitize($request->all());
												if($inputs)
												{
																$save = $this->saveRecord($inputs);
																if($save)
																{
																				return redirect('/'.$nick);
																}
																else
																{
																				$this->showError();
																}
												}												
								}
								else
								{
												$data['person'] = $this->getContact($nick);	
												
												if($data['person'])
												{
																$path = base_path()."/views/detail.php";																			
																$data['array'] = [];

																foreach($this->categories as $key => $item)
																{
																				if(isset($data['person'][$key]) && $data['person'][$key])
																				{
																								$data['array'][$key] = $item;
																				}
																}

																$this->callView($path,	$data);												
												}
												else
												{
																$this->showError();
												}
								}						
				}
				
				/**
				 * Allows user to delete a contact.
				 * @param string $nick Contacts nickname used as unique ID
				 * @return object Instance of Redirector class.
				 */
				public function delete($nick)
				{
								$file = file_get_contents($this->dictionary_file);
								$array = json_decode($file, TRUE);
								
								if(is_array($array))
								{
												if(!array_key_exists($nick,	$array))
												{
																$response = FALSE;
												}
												else
												{
																unset($array[$nick]);
																$response = TRUE;
												}
																					
												$json = json_encode($array);
												$save = file_put_contents($this->dictionary_file,	$json);

												if(!$save)
												{
																$response = FALSE;
												}
								}
								else
								{
												$response = FALSE;
								}
								
								if($response)
								{
												return redirect('/');
								}
								else
								{
												$this->showError();
								}
				}
				
				/**
				 * Finds if a section $id exists and if does, it returns it's parameters.
				 * @param string $id ID of section in $this->categories
				 * @return mixed Array on success, NULL on fault.
				 */
				public function getSection($id)
				{
								if(array_key_exists($id,	$this->categories))
								{
												$response =  $this->categories[$id];
								}
								else
								{
												$response = NULL;
								}
								
								return $response;
				}
				
				/**
				 * Gets all records for a contact with given ID or return FALSE
				 * @param string $nick Nickname of contact used as unique ID
				 * @return mixed Array of data on success, FALSE on fault.
				 */
				public function getContact($nick)
				{
								$array = $this->getList();
								
								if(is_array($array) && array_key_exists($nick,	$array))
								{
												$response = $array[$nick];
								}
								else
								{
												$response = FALSE;
								}
								
								return $response;
				}
				
				/**
				 * Saves a new contact or updates existing one.
				 * @param array $data All data for the contact being created.
				 * @param boolean $new Indicates if the record is supposed to be new.
				 * @return boolean TRUE on success, FALSE on fault
				 */
				public function saveRecord($data, $new = FALSE)
				{																
								$file = file_get_contents($this->dictionary_file);
								$array = json_decode($file, TRUE);

								if(is_array($array))
								{
												if($new)
												{
																if(in_array($data['nickname'],	$array))
																{
																				$this->showError();
																}
																else
																{
																				$array[$data['nickname']] = $data;
																}
												}
												else
												{
																$array[$data['nickname']] = $data;
												}
																								
												$json = json_encode($array, JSON_PRETTY_PRINT);
												$save = file_put_contents($this->dictionary_file,	$json);

												if($save)
												{
																$response = TRUE;
												}
												else
												{
																$response = FALSE;
												}
								}
								else
								{
												$response = FALSE;
								}
								
								return $response;		
				}
				
				/**
				 * Returns a list of all contacts with all their records.
				 * @return mixed Array on success, NULL if db is empty, FALSE on fault.
				 */
				public function getList()
				{
								if(file_exists($this->dictionary_file))
								{												
												$file = file_get_contents($this->dictionary_file);
												$array = json_decode($file, TRUE);		
												
												if(is_array($array))
												{
																$response = $array;
												}
												else
												{
																$response = NULL;																												
												}											
								}																
								else
								{
												$response = FALSE;
								}
								
								return $response;
				}
				
				/**
				 * Includes a php file on a given path, if exists. Otherwise shows error page.
				 * @param string $path absolute filesystem path to the view.
				 * @param mixed $data Any variables, that should be available in the view.
				 */
				private function callView($path, $data = NULL)
				{
								if(file_exists($path))
								{
												include	$path;
								}
								else
								{
												$this->showError();
								}
				}
				
				/**
				 * Shows a page with unspecified error.
				 */
				public function showError()
				{
								$this->callView(base_path()."/views/error.php");
				}
				
				
				/**
				 * Sanitizes user input. (if it is a numeric, string or an array containing one of those).
				 * Removes whitespaces, html tags, semicolons and converts double quotes to single quotes.
				 * @param mixed $input Any user input
				 * @return mixed Sanitized input on success, FALSE otherwise.
				 */
				public function sanitize($input)
				{
								$response = FALSE;
								
								if($input)
								{
												if(is_string($input) || is_numeric($input))
												{
																				$trimmed = trim($input);
																				$stripped = strip_tags($trimmed); 
																				$escaped = htmlspecialchars($stripped);
																				$response = str_replace(';', '', $escaped);
												}
												elseif(is_array($input))
												{
																foreach($input as $key => $item)
																{
																				$response[$key] = $this->sanitize($item);																				
																}
												}
												else
												{
																$response = FALSE;
												}								
								}
								
								return $response;
				}
}
